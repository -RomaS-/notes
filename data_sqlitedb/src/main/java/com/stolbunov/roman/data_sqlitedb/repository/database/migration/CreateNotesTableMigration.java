package com.stolbunov.roman.data_sqlitedb.repository.database.migration;

import android.database.sqlite.SQLiteDatabase;

import javax.inject.Inject;

public class CreateNotesTableMigration implements IMigration {

    @Inject
    CreateNotesTableMigration() {
    }

    @Override
    public int getTargetVersion() {
        return 1;
    }

    @Override
    public void execute(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE notes (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, " +
                "description TEXT, " +
                "priority INTEGER, " +
                "creation_date INTEGER, " +
                "expiration_date INTEGER)");
    }
}

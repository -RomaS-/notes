package com.stolbunov.roman.data_sqlitedb.repository;

import android.text.TextUtils;

import com.stolbunov.roman.data_sqlitedb.repository.database.NoteDatabase;
import com.stolbunov.roman.data_sqlitedb.mapper.NoteMapper;
import com.stolbunov.roman.domain.entities.Note;
import com.stolbunov.roman.domain.repository.INoteRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class SQLiteNoteRepository implements INoteRepository {
    private NoteDatabase repository;

    @Inject
    SQLiteNoteRepository(NoteDatabase repository) {
        this.repository = repository;
    }

    @Override
    public Completable save(Note note) {
        return Single.just(note)
                .map(NoteMapper::transform)
                .flatMapCompletable(
                        noteEntity -> (TextUtils.isEmpty(noteEntity.getId()))
                                ? repository.add(noteEntity) : repository.update(noteEntity));
    }

    @Override
    public Completable remove(String noteId) {
        return Single.just(noteId)
                .flatMapCompletable(repository::remove);
    }

    @Override
    public Single<List<Note>> getNotes() {
        return repository.getNotes()
                .map(NoteMapper::transformList);
    }

    @Override
    public Single<Note> getNote(String noteId) {
        return Single.just(noteId)
                .flatMap(repository::getNote)
                .map(NoteMapper::transform);
    }
}

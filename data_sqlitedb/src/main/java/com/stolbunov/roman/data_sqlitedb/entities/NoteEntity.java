package com.stolbunov.roman.data_sqlitedb.entities;

public class NoteEntity {
    private String id;
    private String title;
    private String description;
    private int priority;
    private long creationDate;
    private long expirationDate;

    public NoteEntity(String id, String title, String description, int priority, long creationDate,
                      long expirationDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.creationDate = creationDate;
        this.expirationDate = expirationDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }

    public String getId() {
        return id;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public long getExpirationDate() {
        return expirationDate;
    }
}

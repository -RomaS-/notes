package com.stolbunov.roman.data_sqlitedb.authorization;


import com.stolbunov.roman.domain.entities.Credentials;
import com.stolbunov.roman.domain.repository.INoteRepositoryAuth;

import javax.inject.Inject;

import io.reactivex.Single;

public class NoteSQLiteAuth implements INoteRepositoryAuth {

    @Inject
    public NoteSQLiteAuth() {
    }

    @Override
    public Single<Boolean> signInWithEmailAndPassword(Credentials credentials) {
        return Single.just(true);
    }

    @Override
    public Single<Boolean> createUserWithEmailAndPassword(Credentials credentials) {
        return Single.just(true);
    }

    @Override
    public boolean isCurrentUser() {
        return true;
    }

    @Override
    public boolean isShowDetailsToWorkWithUser() {
        return false;
    }

    @Override
    public void signOut() {
    }
}

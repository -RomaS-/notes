package com.stolbunov.roman.data_sqlitedb.mapper;

import com.stolbunov.roman.data_sqlitedb.entities.NoteEntity;
import com.stolbunov.roman.domain.entities.Note;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.stolbunov.roman.domain.entities.NotePriority;

public class NoteMapper {
    public static NoteEntity transform(Note note) {
        return new NoteEntity(
                note.getId(),
                note.getTitle(),
                note.getDescription(),
                note.getPriority().ordinal(),
                note.getCreationDate().getTime(),
                note.getExpirationDate().getTime());
    }

    public static Note transform(NoteEntity noteEntity) {
        return new Note(
                String.valueOf(noteEntity.getId()),
                noteEntity.getTitle(),
                noteEntity.getDescription(),
                NotePriority.values()[noteEntity.getPriority()],
                new Date(noteEntity.getCreationDate()),
                new Date(noteEntity.getExpirationDate()));
    }

    public static List<Note> transformList(List<NoteEntity> notes) {
        List<Note> result = new ArrayList<>(notes.size());
        for (NoteEntity noteEntity : notes) {
            result.add(transform(noteEntity));
        }
        return result;
    }
}

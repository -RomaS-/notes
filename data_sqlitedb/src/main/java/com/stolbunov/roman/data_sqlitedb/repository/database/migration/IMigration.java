package com.stolbunov.roman.data_sqlitedb.repository.database.migration;

import android.database.sqlite.SQLiteDatabase;

public interface IMigration {
    int getTargetVersion();

    void execute(SQLiteDatabase database);
}

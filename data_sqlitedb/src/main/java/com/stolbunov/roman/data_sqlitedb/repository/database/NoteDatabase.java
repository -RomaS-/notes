package com.stolbunov.roman.data_sqlitedb.repository.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.stolbunov.roman.data_sqlitedb.entities.NoteEntity;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class NoteDatabase {
    private static final String TABLE_NOTES = "notes";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_PRIORITY = "priority";
    private static final String COLUMN_CREATION_DATE = "creation_date";
    private static final String COLUMN_EXPIRATION_DATE = "expiration_date";

    private final Lock readLock;
    private final Lock writeLock;

    private final SQLiteDatabase writableDatabase;
    private final SQLiteDatabase readableDatabase;

    @Inject
    NoteDatabase(DatabaseHelper databaseHelper, ReentrantReadWriteLock readWriteLock) {
        writableDatabase = databaseHelper.getWritableDatabase();
        readableDatabase = databaseHelper.getReadableDatabase();

        readLock = readWriteLock.readLock();
        writeLock = readWriteLock.writeLock();
    }

    public Completable add(NoteEntity note) {
        return Completable.fromAction(() -> addNewNote(note));
    }

    public Completable update(NoteEntity note) {
        return Completable.fromAction(() -> updateNote(note));
    }

    public Single<List<NoteEntity>> getNotes() {
        return Single.fromCallable(this::getListNotes);
    }

    public Single<NoteEntity> getNote(String noteId) {
        return Single.fromCallable(() -> getNoteById(noteId));
    }

    public Completable remove(String noteId) {
        return Completable.fromAction(() -> removeNote(noteId));
    }

    private void addNewNote(NoteEntity noteEntity) {
        writeLock.lock();
        try {
            writableDatabase.insert(TABLE_NOTES, null, getContentValues(noteEntity));
        } finally {
            writeLock.unlock();
        }

    }

    private void updateNote(NoteEntity noteEntity) {
        writeLock.lock();
        try {
            String whereClause = COLUMN_ID + " = ?";
            String[] whereArgs = new String[]{noteEntity.getId()};

            writableDatabase.update(TABLE_NOTES, getContentValues(noteEntity), whereClause, whereArgs);
        } finally {
            writeLock.unlock();
        }
    }

    private List<NoteEntity> getListNotes() {
        readLock.lock();
        List<NoteEntity> notes = new LinkedList<>();
        try {
            String query = "SELECT * FROM " + TABLE_NOTES;

            Cursor cursor = readableDatabase.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    notes.add(this.getNoteFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        } finally {
            readLock.unlock();
        }
        return notes;
    }


    private NoteEntity getNoteById(String noteId) {
        NoteEntity noteEntity = null;

        readLock.lock();
        try {
            String query = "SELECT * FROM " + TABLE_NOTES + " WHERE " + COLUMN_ID + " =?";
            String[] args = new String[]{noteId};

            Cursor cursor = readableDatabase.rawQuery(query, args);
            if (cursor.moveToFirst()) {
                noteEntity = getNoteFromCursor(cursor);
            }
        } finally {
            readLock.unlock();
        }
        return noteEntity;
    }


    private void removeNote(String noteId) {
        writeLock.lock();
        try {
            String whereClause = COLUMN_ID + " = ?";
            String[] whereArgs = new String[]{noteId};

            writableDatabase.delete(TABLE_NOTES, whereClause, whereArgs);
        } finally {
            writeLock.unlock();
        }
    }

    private NoteEntity getNoteFromCursor(Cursor cursor) {
        return new NoteEntity(
                cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                cursor.getInt(cursor.getColumnIndex(COLUMN_PRIORITY)),
                cursor.getLong(cursor.getColumnIndex(COLUMN_CREATION_DATE)),
                cursor.getLong(cursor.getColumnIndex(COLUMN_EXPIRATION_DATE)));
    }

    private ContentValues getContentValues(NoteEntity note) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, note.getTitle());
        values.put(COLUMN_DESCRIPTION, note.getDescription());
        values.put(COLUMN_PRIORITY, note.getPriority());
        values.put(COLUMN_CREATION_DATE, note.getCreationDate());
        values.put(COLUMN_EXPIRATION_DATE, note.getExpirationDate());
        return values;
    }
}

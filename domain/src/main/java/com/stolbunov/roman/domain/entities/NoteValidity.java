package com.stolbunov.roman.domain.entities;

public enum NoteValidity {
    VALID,
    INVALID
}

package com.stolbunov.roman.domain.interactor;

import com.stolbunov.roman.domain.entities.Note;
import com.stolbunov.roman.domain.repository.INoteRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class NoteInteractor implements INoteInteractor {
    private INoteRepository repository;

    @Inject
    public NoteInteractor(INoteRepository repository) {
        this.repository = repository;
    }

    @Override
    public Completable save(Note note) {
        return repository.save(note);
    }

    @Override
    public Completable remove(String noteId) {
        return repository.remove(noteId);
    }

    @Override
    public Single<Note> getNote(String noteId) {
        return repository.getNote(noteId);
    }

    @Override
    public Single<List<Note>> getNotesList() {
        return repository.getNotes();
    }
}

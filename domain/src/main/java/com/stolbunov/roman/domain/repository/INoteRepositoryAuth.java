package com.stolbunov.roman.domain.repository;

import com.stolbunov.roman.domain.entities.Credentials;

import io.reactivex.Single;

public interface INoteRepositoryAuth {
    Single<Boolean> signInWithEmailAndPassword(Credentials credentials);

    Single<Boolean> createUserWithEmailAndPassword(Credentials credentials);

    boolean isCurrentUser();

    boolean isShowDetailsToWorkWithUser();

    void signOut();
}

package com.stolbunov.roman.domain.entities;

public enum NotePriority {
    HIGH,
    MEDIUM,
    LOW
}

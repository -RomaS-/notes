package com.stolbunov.roman.domain.entities;

import java.util.Date;
import java.util.Objects;

public class Note {
    private final String id;
    private final String title;
    private final String description;
    private final NotePriority priority;
    private final Date creationDate;
    private final Date expirationDate;
    private final NoteValidity validity;

    public Note(String id, String title, String description, NotePriority priority, Date creationDate,
                Date expirationDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.creationDate = creationDate;
        this.expirationDate = expirationDate;
        validity = new Date().before(expirationDate) ? NoteValidity.VALID : NoteValidity.INVALID;
    }

    public NoteValidity getValidity() {
        return validity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Note)) return false;
        Note note = (Note) o;
        return Objects.equals(getId(), note.getId()) &&
                Objects.equals(getTitle(), note.getTitle()) &&
                Objects.equals(getDescription(), note.getDescription()) &&
                getPriority() == note.getPriority() &&
                Objects.equals(getCreationDate(), note.getCreationDate()) &&
                Objects.equals(getExpirationDate(), note.getExpirationDate()) &&
                getValidity() == note.getValidity();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getDescription(), getPriority(),
                getCreationDate(), getExpirationDate(), getValidity());
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public NotePriority getPriority() {
        return priority;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

}
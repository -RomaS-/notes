package com.stolbunov.roman.domain.interactor;

import com.stolbunov.roman.domain.entities.Credentials;
import com.stolbunov.roman.domain.repository.INoteRepositoryAuth;

import javax.inject.Inject;

import io.reactivex.Single;

public class AuthorizationInteractor {
    private INoteRepositoryAuth repository;

    @Inject
    public AuthorizationInteractor(INoteRepositoryAuth repository) {
        this.repository = repository;
    }

    public Single<Boolean> signInWithEmailAndPassword(Credentials credentials) {
        return repository.signInWithEmailAndPassword(credentials);
    }

    public Single<Boolean> createUserWithEmailAndPassword(Credentials credentials) {
        return repository.createUserWithEmailAndPassword(credentials);
    }

    public boolean isCurrentUser() {
        return repository.isCurrentUser();
    }

    public boolean isShowDetailsToWorkWithUser() {
        return repository.isShowDetailsToWorkWithUser();
    }

    public void signOut() {
        repository.signOut();
    }
}

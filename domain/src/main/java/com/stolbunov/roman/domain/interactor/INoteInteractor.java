package com.stolbunov.roman.domain.interactor;

import com.stolbunov.roman.domain.entities.Note;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface INoteInteractor {
    Completable save(Note note);

    Completable remove(String noteId);

    Single<Note> getNote(String noteId);

    Single<List<Note>> getNotesList();
}

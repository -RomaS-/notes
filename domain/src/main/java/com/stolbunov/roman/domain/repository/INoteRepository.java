package com.stolbunov.roman.domain.repository;

import com.stolbunov.roman.domain.entities.Note;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface INoteRepository {
    Completable save(Note note);

    Completable remove(String noteId);

    Single<List<Note>> getNotes();

    Single<Note> getNote(String noteId);
}

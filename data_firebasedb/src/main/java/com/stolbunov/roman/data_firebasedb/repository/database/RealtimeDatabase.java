package com.stolbunov.roman.data_firebasedb.repository.database;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.stolbunov.roman.data_firebasedb.utils.RxFirebase;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class RealtimeDatabase {

    private static final String PATH_TO_NOTE = "/notes/%s/notes/%s";
    private static final String PATH_TO_NOTES = "/notes/%s/notes";

    @Inject
    public RealtimeDatabase() {
    }

    public Completable add(Map<String, Object> noteMap) {
        return Completable.fromAction(() -> addNote(noteMap));
    }

    public Completable remove(String noteId) {
        return Completable.fromAction(() -> removeNoteById(noteId));
    }

    public Single<DataSnapshot> getNotes() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        return RxFirebase
                .singleValueEventObserver(
                        FirebaseDatabase
                                .getInstance()
                                .getReference(String.format(PATH_TO_NOTES, user.getUid())));
    }

    public Single<DataSnapshot> getNote(String noteId) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        return RxFirebase.singleValueEventObserver(
                FirebaseDatabase
                        .getInstance()
                        .getReference(String.format(PATH_TO_NOTE, user.getUid(), noteId)));
    }

    private void addNote(Map<String, Object> noteMap) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Map<String, Object> result = new HashMap<>();
        result.put(String.format(PATH_TO_NOTE, user.getUid(), noteMap.get("id")), noteMap);

        Log.d("nik", "addNote: " + user.getUid());
        FirebaseDatabase
                .getInstance()
                .getReference()
                .updateChildren(result);
    }

    private void removeNoteById(String noteId) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase
                .getInstance()
                .getReference(String.format(PATH_TO_NOTE, user.getUid(), noteId))
                .removeValue();
    }
}

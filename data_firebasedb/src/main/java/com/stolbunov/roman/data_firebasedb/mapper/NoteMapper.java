package com.stolbunov.roman.data_firebasedb.mapper;

import android.text.TextUtils;

import com.google.firebase.database.DataSnapshot;
import com.stolbunov.roman.data_firebasedb.entities.NoteEntity;
import com.stolbunov.roman.domain.entities.Note;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class NoteMapper {

    public static Map<String, Object> transformToMap(Note note) {
        Map<String, Object> noteMap = new HashMap<>();

        if (TextUtils.isEmpty(note.getId())) {
            String uuid = UUID.randomUUID().toString();
            noteMap.put("id", uuid);
        } else {
            noteMap.put("id", note.getId());
        }

        noteMap.put("title", note.getTitle());
        noteMap.put("description", note.getDescription());
        noteMap.put("priority", note.getPriority());
        noteMap.put("creationDate", note.getCreationDate());
        noteMap.put("expirationDate", note.getExpirationDate());
        return noteMap;
    }

    public static Note transformToNote(NoteEntity noteEntity) {
        return new Note(
                noteEntity.getId(),
                noteEntity.getTitle(),
                noteEntity.getDescription(),
                noteEntity.getPriority(),
                noteEntity.getCreationDate(),
                noteEntity.getExpirationDate());
    }

    public static List<Note> noteEntityListToNoteList(List<NoteEntity> notes) {
        List<Note> result = new ArrayList<>(notes.size());
        for (NoteEntity noteEntity : notes) {
            result.add(transformToNote(noteEntity));
        }
        return result;
    }

    public static List<NoteEntity> dataSnapshotToNoteEntityList(DataSnapshot dataSnapshot) {
        List<NoteEntity> list = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            list.add(snapshot.getValue(NoteEntity.class));
        }
        return list;
    }

    public static Note dataSnapshotToNote(DataSnapshot dataSnapshot) {
        NoteEntity noteEntity = dataSnapshot.getValue(NoteEntity.class);
        return transformToNote(noteEntity);
    }
}

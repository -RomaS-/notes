package com.stolbunov.roman.data_firebasedb.entities;

import com.google.firebase.database.IgnoreExtraProperties;
import com.stolbunov.roman.domain.entities.NotePriority;

import java.util.Date;

@IgnoreExtraProperties
public class NoteEntity {
    private String id;
    private String title;
    private String description;
    private NotePriority priority;
    private Date creationDate;
    private Date expirationDate;

    public NoteEntity() {
    }

    public NoteEntity(String id, String title, String description, NotePriority priority, Date creationDate,
                      Date expirationDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.creationDate = creationDate;
        this.expirationDate = expirationDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public NotePriority getPriority() {
        return priority;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }
}

package com.stolbunov.roman.data_firebasedb.authorization;

import com.google.firebase.auth.FirebaseAuth;
import com.stolbunov.roman.domain.entities.Credentials;
import com.stolbunov.roman.domain.repository.INoteRepositoryAuth;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;

public class NoteFirebaseAuth implements INoteRepositoryAuth {

    @Inject
    public NoteFirebaseAuth() {
    }

    @Override
    public Single<Boolean> signInWithEmailAndPassword(Credentials credentials) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        return Single.create((SingleEmitter<Boolean> emitter) -> {
            auth.signInWithEmailAndPassword(credentials.getEmail(), credentials.getPassword())
                    .addOnCompleteListener(task -> {
                        try {
                            emitter.onSuccess(task.isSuccessful());
                        } catch (Exception e) {
                            emitter.onError(e);
                        }
                    });
        });
    }

    @Override
    public Single<Boolean> createUserWithEmailAndPassword(Credentials credentials) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        return Single.create((SingleEmitter<Boolean> emitter) -> {
            auth.createUserWithEmailAndPassword(credentials.getEmail(), credentials.getPassword())
                    .addOnCompleteListener(task -> {
                        try {
                            emitter.onSuccess(task.isSuccessful());
                        } catch (Exception e) {
                            emitter.onError(e);
                        }
                    });
        });
    }

    @Override
    public void signOut() {
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    public boolean isCurrentUser() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        return auth.getCurrentUser() != null;
    }

    @Override
    public boolean isShowDetailsToWorkWithUser() {
        return true;
    }


}

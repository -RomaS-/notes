package com.stolbunov.roman.data_firebasedb.repository;

import com.stolbunov.roman.data_firebasedb.mapper.NoteMapper;
import com.stolbunov.roman.data_firebasedb.repository.database.RealtimeDatabase;
import com.stolbunov.roman.domain.entities.Note;
import com.stolbunov.roman.domain.repository.INoteRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class FirebaseRepository implements INoteRepository {
    private RealtimeDatabase database;

    @Inject
    public FirebaseRepository(RealtimeDatabase database) {
        this.database = database;
    }

    @Override
    public Completable save(Note note) {
        return Single.just(note)
                .map(NoteMapper::transformToMap)
                .flatMapCompletable(database::add);
    }

    @Override
    public Completable remove(String noteId) {
        return database.remove(noteId);
    }

    @Override
    public Single<List<Note>> getNotes() {
        return database.getNotes()
                .map(NoteMapper::dataSnapshotToNoteEntityList)
                .map(NoteMapper::noteEntityListToNoteList);
    }

    @Override
    public Single<Note> getNote(String noteId) {
        return Single.just(noteId)
                .flatMap(database::getNote)
                .map(NoteMapper::dataSnapshotToNote);
    }
}
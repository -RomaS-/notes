package com.stolbunov.roman.notes.ui.screens.editor;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.stolbunov.roman.notes.R;
import com.stolbunov.roman.notes.databinding.FragmentDialogDateBinding;

import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

public class DateDialogFragment extends DialogFragment {
    private FragmentDialogDateBinding binding;
    private DateDialogFragmentViewModel vm;
    private OnSaveClickListener listener;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnSaveClickListener) {
            listener = (OnSaveClickListener) context;
        } else {
            throw new ClassCastException("Implement OnSaveClickListener");
        }
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnSaveClickListener) {
            listener = (OnSaveClickListener) activity;
        } else {
            throw new ClassCastException("Implement OnSaveClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public static DateDialogFragment getInstance() {
        return new DateDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        vm = ViewModelProviders
                .of(this)
                .get(DateDialogFragmentViewModel.class);

        dataBinding(inflater, container);
        setTransparentBackground();
        return binding.getRoot();
    }

    private void dataBinding(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_dialog_date,
                container,
                false);

        binding.setLifecycleOwner(this);
        binding.setVm(vm);

        binding.timePicker.setIs24HourView(true);

        binding.save.setOnClickListener(this::save);
        binding.cancel.setOnClickListener(this::cancel);
    }

    private void save(View view) {
        Date date = getFullDate();
        if (listener != null) {
            listener.onSaveClick(date);
        }
        dismiss();
    }

    private void cancel(View view) {
        dismiss();
    }


    private void setTransparentBackground() {
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    private Date getFullDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, binding.datePicker.getDayOfMonth());
        calendar.set(Calendar.MONTH, binding.datePicker.getMonth());
        calendar.set(Calendar.YEAR, binding.datePicker.getYear());
        calendar.set(Calendar.HOUR, binding.timePicker.getCurrentHour());
        calendar.set(Calendar.MINUTE, binding.timePicker.getCurrentMinute());
        return calendar.getTime();
    }

    public interface OnSaveClickListener {
        void onSaveClick(Date date);
    }
}

package com.stolbunov.roman.notes.ui.screens.list;

import com.stolbunov.roman.domain.entities.Note;

import java.util.List;

public interface INoteListAdapter {
    void setDataList(List<Note> list);
}

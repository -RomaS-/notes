package com.stolbunov.roman.notes.di;

import com.stolbunov.roman.notes.di.scope.ActivityScope;
import com.stolbunov.roman.notes.ui.screens.editor.NoteEditorActivity;
import com.stolbunov.roman.notes.ui.screens.list.NotesListActivity;
import com.stolbunov.roman.notes.ui.screens.login.LoginActivity;
import com.stolbunov.roman.notes.ui.screens.statistics.NoteStatisticsActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = AndroidSupportInjectionModule.class)
interface DaggerAndroidModule {

    @ContributesAndroidInjector
    LoginActivity loginActitity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {NotesListModule.class})
    NotesListActivity mainActivity();

    @ContributesAndroidInjector(modules = {NoteEditorModule.class})
    NoteEditorActivity noteEditorActivity();

    @ContributesAndroidInjector
    NoteStatisticsActivity noteStatisticsActivity();
}

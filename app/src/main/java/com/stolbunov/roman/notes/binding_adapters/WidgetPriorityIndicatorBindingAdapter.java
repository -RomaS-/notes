package com.stolbunov.roman.notes.binding_adapters;

import com.stolbunov.roman.notes.ui.widgets.WidgetPriorityIndicator;

import androidx.databinding.BindingAdapter;

import com.stolbunov.roman.domain.entities.NotePriority;
import com.stolbunov.roman.domain.entities.NoteValidity;

public class WidgetPriorityIndicatorBindingAdapter {

    @BindingAdapter(value = {"priority", "validity"})
    public static void setAppearance(WidgetPriorityIndicator widget, NotePriority priority, NoteValidity status) {
        if (priority != null && status != null) {
            switch (status) {
                case VALID:
                    widget.setBackgroundByPriority(priority);
                    break;
                case INVALID:
                    widget.setIndicatorColorByPriority(priority);
                    break;
            }

        }
    }
}

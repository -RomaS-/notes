package com.stolbunov.roman.notes.ui.screens.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stolbunov.roman.domain.entities.Note;
import com.stolbunov.roman.domain.entities.NoteValidity;
import com.stolbunov.roman.notes.R;
import com.stolbunov.roman.notes.databinding.ItemNoteListInvalidBinding;
import com.stolbunov.roman.notes.databinding.ItemNoteListValidBinding;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class NotesListAdapter extends RecyclerView.Adapter<NotesListAdapter.BaseNoteViewHolder>
        implements INoteListAdapter {

    private List<Note> data;
    private OnRemoveNoteListener removeListener;
    private OnItemClickListener itemClickListener;

    @Inject
    public NotesListAdapter() {
        data = Collections.emptyList();
    }

    abstract class BaseNoteViewHolder extends RecyclerView.ViewHolder {
        BaseNoteViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        abstract void bind(Note note);
    }

    class NoteValidViewHolder extends BaseNoteViewHolder {
        private ItemNoteListValidBinding binding;

        NoteValidViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        @Override
        void bind(Note note) {
            binding.setNote(note);
        }
    }

    class NoteInvalidViewHolder extends BaseNoteViewHolder {
        private ItemNoteListInvalidBinding binding;

        NoteInvalidViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        @Override
        void bind(Note note) {
            binding.setNote(note);
        }
    }

    void removeItem(int position) {
        if (removeListener != null) {
            removeListener.onDeleteItemClick(data.get(position).getId());
        }
    }

    @Override
    public void setDataList(List<Note> notes) {
        data = notes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        Note note = data.get(position);
        return note.getValidity().ordinal();
    }

    @NonNull
    @Override
    public BaseNoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        switch (NoteValidity.values()[itemType]) {
            case VALID:
                return new NoteValidViewHolder(createView(R.layout.item_note_list_valid, viewGroup));
            case INVALID:
                return new NoteInvalidViewHolder(createView(R.layout.item_note_list_invalid, viewGroup));
            default:
                throw new IllegalArgumentException("Wrong value: \"itemType = " + itemType + "\"");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseNoteViewHolder holder, int position) {
        Note note = data.get(position);
        holder.itemView.setOnClickListener(view -> onItemClick(note));
        holder.bind(note);
    }

    private View createView(int layoutId, ViewGroup viewGroup) {
        return DataBindingUtil.inflate(
                LayoutInflater.from(viewGroup.getContext()),
                layoutId,
                viewGroup,
                false)
                .getRoot();
    }

    public void setRemoveListener(OnRemoveNoteListener removeListener) {
        this.removeListener = removeListener;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    private void onItemClick(Note note) {
        if (itemClickListener != null) {
            itemClickListener.onItemClick(note.getId());
        }
    }

    public interface OnRemoveNoteListener {
        void onDeleteItemClick(String noteId);
    }

    public interface OnItemClickListener {
        void onItemClick(String noteId);
    }
}
package com.stolbunov.roman.notes.ui.screens.statistics;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class NoteStatisticsViewModelFactory implements ViewModelProvider.Factory {
    private NoteStatisticsViewModel vm;

    @Inject
    public NoteStatisticsViewModelFactory(NoteStatisticsViewModel vm) {
        this.vm = vm;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        //noinspection unchecked
        return (T) vm;
    }
}
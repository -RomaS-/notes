package com.stolbunov.roman.notes.ui.screens.editor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.stolbunov.roman.notes.R;
import com.stolbunov.roman.notes.databinding.ActivityNoteEditorBinding;
import com.stolbunov.roman.notes.entities.Event;
import com.stolbunov.roman.notes.entities.Nothing;
import com.stolbunov.roman.notes.ui.screens.list.NotesListActivity;

import java.util.Date;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerAppCompatActivity;

public class NoteEditorActivity extends DaggerAppCompatActivity implements DateDialogFragment.OnSaveClickListener {
    private static final String KEY_INTENT_NOTE_ID = "KEY_INTENT_NOTE_ID";

    private NoteEditorViewModel vm;

    @Inject
    NoteEditorViewModelFactory viewModelFactory;

    public static Intent getIntent(Context context, String noteId) {
        Intent intent = new Intent(context, NoteEditorActivity.class);
        intent.putExtra(KEY_INTENT_NOTE_ID, noteId);
        return intent;
    }

    public String getNoteId() {
        return getIntent().getStringExtra(KEY_INTENT_NOTE_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vm = ViewModelProviders
                .of(this, viewModelFactory)
                .get(NoteEditorViewModel.class);

        ActivityNoteEditorBinding binding = DataBindingUtil
                .setContentView(this, R.layout.activity_note_editor);
        binding.setLifecycleOwner(this);
        binding.setVm(vm);
        binding.showDateCreationDialog.setOnClickListener(this::showDateCreationDialog);

        subscriptionsToLiveData();
    }

    private void subscriptionsToLiveData() {
        vm.getShowMessage().observe(this, this::showMessage);
        vm.getIsSuccessfulUpdate().observe(this, this::editNote);
    }

    private void showMessage(Event<String> event) {
        if (!event.isHandled()) {
            Toast.makeText(this, event.getValue(), Toast.LENGTH_SHORT).show();
        }
    }

    private void editNote(Event<Nothing> event) {
        if (!event.isHandled() && event.getValue() != null) {
            Intent intent = NotesListActivity.getIntent(this);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    public void showDateCreationDialog(View view) {
        DateDialogFragment.getInstance()
                .show(getSupportFragmentManager(), "DateDialogFragment");
    }

    @Override
    public void onSaveClick(Date date) {
        vm.setDate(date);
    }
}

package com.stolbunov.roman.notes.di;

import com.stolbunov.roman.notes.di.scope.ActivityScope;
import com.stolbunov.roman.notes.ui.screens.list.NotesListAdapter;
import com.stolbunov.roman.notes.ui.screens.list.SwipeDeleteItemCallback;

import androidx.recyclerview.widget.ItemTouchHelper;
import dagger.Module;
import dagger.Provides;

@Module
class NotesListModule {

    @Provides
    ItemTouchHelper provideItemTouchHelper(SwipeDeleteItemCallback callback) {
        return new ItemTouchHelper(callback);
    }

    @ActivityScope
    @Provides
    NotesListAdapter provideNoteListAdapter() {
        return new NotesListAdapter();
    }
}

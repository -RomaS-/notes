package com.stolbunov.roman.notes.entities;

public class NoteStatistics {
    private int numberHighPriority;
    private int numberMediumPriority;
    private int numberLowPriority;
    private int numberInvalid;
    private int numberInvalidInFuture;
    private int numberChangedOnMonday;
    private int numberChangedOnTuesday;
    private int numberChangedOnWednesday;
    private int numberChangedToThursday;
    private int numberChangedToFriday;
    private int numberChangedToSaturday;
    private int numberChangedToSunday;

    private NoteStatistics() {
    }

    public static class Builder {
        private NoteStatistics noteStatistics;

        public Builder() {
            noteStatistics = new NoteStatistics();
        }

        public Builder numberHighPriority(int numberHighPriority) {
            noteStatistics.numberHighPriority = numberHighPriority;
            return this;
        }

        public Builder numberMediumPriority(int numberMediumPriority) {
            noteStatistics.numberMediumPriority = numberMediumPriority;
            return this;
        }

        public Builder numberLowPriority(int numberLowPriority) {
            noteStatistics.numberLowPriority = numberLowPriority;
            return this;
        }

        public Builder numberInvalid(int numberInvalid) {
            noteStatistics.numberInvalid = numberInvalid;
            return this;
        }

        public Builder numberInvalidInFuture(int numberInvalidInFuture) {
            noteStatistics.numberInvalidInFuture = numberInvalidInFuture;
            return this;
        }

        public Builder numberChangedOnMonday(int numberChangedOnMonday) {
            noteStatistics.numberChangedOnMonday = numberChangedOnMonday;
            return this;
        }

        public Builder numberChangedOnTuesday(int numberChangedOnTuesday) {
            noteStatistics.numberChangedOnTuesday = numberChangedOnTuesday;
            return this;
        }

        public Builder numberChangedOnWednesday(int numberChangedOnWednesday) {
            noteStatistics.numberChangedOnWednesday = numberChangedOnWednesday;
            return this;
        }

        public Builder numberChangedToThursday(int numberChangedToThursday) {
            noteStatistics.numberChangedToThursday = numberChangedToThursday;
            return this;
        }

        public Builder numberChangedToFriday(int numberChangedToFriday) {
            noteStatistics.numberChangedToFriday = numberChangedToFriday;
            return this;
        }

        public Builder numberChangedToSaturday(int numberChangedToSaturday) {
            noteStatistics.numberChangedToSaturday = numberChangedToSaturday;
            return this;
        }

        public Builder numberChangedToSunday(int numberChangedToSunday) {
            noteStatistics.numberChangedToSunday = numberChangedToSunday;
            return this;
        }

        public NoteStatistics build(){
            return noteStatistics;
        }
    }

    public int getNumberHighPriority() {
        return numberHighPriority;
    }

    public int getNumberMediumPriority() {
        return numberMediumPriority;
    }

    public int getNumberLowPriority() {
        return numberLowPriority;
    }

    public int getNumberInvalid() {
        return numberInvalid;
    }

    public int getNumberInvalidInFuture() {
        return numberInvalidInFuture;
    }

    public int getNumberChangedOnMonday() {
        return numberChangedOnMonday;
    }

    public int getNumberChangedOnTuesday() {
        return numberChangedOnTuesday;
    }

    public int getNumberChangedOnWednesday() {
        return numberChangedOnWednesday;
    }

    public int getNumberChangedToThursday() {
        return numberChangedToThursday;
    }

    public int getNumberChangedToFriday() {
        return numberChangedToFriday;
    }

    public int getNumberChangedToSaturday() {
        return numberChangedToSaturday;
    }

    public int getNumberChangedToSunday() {
        return numberChangedToSunday;
    }
}

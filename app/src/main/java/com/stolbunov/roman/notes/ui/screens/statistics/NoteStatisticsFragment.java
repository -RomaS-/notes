package com.stolbunov.roman.notes.ui.screens.statistics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stolbunov.roman.notes.R;
import com.stolbunov.roman.notes.databinding.FragmentNoteStatisticsBinding;
import com.stolbunov.roman.notes.entities.NoteStatistics;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

public class NoteStatisticsFragment extends Fragment {
    private OnClickListener listener;
    private FragmentNoteStatisticsBinding binding;

    public static NoteStatisticsFragment newInstance() {
        return new NoteStatisticsFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnClickListener) {
            listener = (OnClickListener) context;
        } else {
            throw new ClassCastException("Implement OnClickListener");
        }
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnClickListener) {
            listener = (OnClickListener) activity;
        } else {
            throw new ClassCastException("Implement OnClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_note_statistics,
                container,
                false);

        binding.setLifecycleOwner(this);

        binding.hideInfo.setOnClickListener(this::onHideClick);
        return binding.getRoot();
    }

    public void setNoteStatistics(NoteStatistics info) {
        binding.setInfo(info);
    }

    public void onHideClick(View view) {
        if (listener != null) {
            listener.onHideClick();
        }
    }

    public interface OnClickListener {
        void onHideClick();
    }
}

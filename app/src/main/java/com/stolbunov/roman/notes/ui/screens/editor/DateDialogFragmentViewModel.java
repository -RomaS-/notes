package com.stolbunov.roman.notes.ui.screens.editor;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DateDialogFragmentViewModel extends ViewModel {
    private MutableLiveData<Boolean> isShowDatePicker = new MutableLiveData<>();
    private MutableLiveData<Boolean> isShowTimePicker = new MutableLiveData<>();

    public DateDialogFragmentViewModel() {
        showDatePicker();
    }

    public void showDatePicker() {
        isShowDatePicker.setValue(true);
        isShowTimePicker.setValue(false);
    }

    public void showTimePicker() {
        isShowTimePicker.setValue(true);
        isShowDatePicker.setValue(false);
    }

    public LiveData<Boolean> getIsShowDatePicker() {
        return isShowDatePicker;
    }

    public LiveData<Boolean> getIsShowTimePicker() {
        return isShowTimePicker;
    }
}

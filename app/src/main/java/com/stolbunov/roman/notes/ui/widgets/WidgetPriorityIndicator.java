package com.stolbunov.roman.notes.ui.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.stolbunov.roman.domain.entities.NotePriority;
import com.stolbunov.roman.notes.R;
import com.stolbunov.roman.notes.databinding.WidgetPriorityIndicatorBinding;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class WidgetPriorityIndicator extends FrameLayout {
    private WidgetPriorityIndicatorBinding binding;

    public WidgetPriorityIndicator(@NonNull Context context) {
        this(context, null);
    }

    public WidgetPriorityIndicator(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WidgetPriorityIndicator(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, AttributeSet attrs, int defStyleAttr) {
        binding = DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.widget_priority_indicator,
                this,
                true);

        TypedArray typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.WidgetPriorityIndicator,
                defStyleAttr,
                R.style.DefaultWidgetStyle);

        setStrokeColor(typedArray.getColorStateList(R.styleable.WidgetPriorityIndicator_stroke_color));
        setCenterColor(typedArray.getColorStateList(R.styleable.WidgetPriorityIndicator_center_color));
        setCenterImage(typedArray.getResourceId(R.styleable.WidgetPriorityIndicator_android_src, defStyleAttr));
        setIndicatorColor(typedArray.getColorStateList(R.styleable.WidgetPriorityIndicator_indicator_color));
        typedArray.recycle();
    }

    public void setStrokeColor(ColorStateList tint) {
        binding.container.setBackgroundTintList(tint);
    }

    public void setCenterColor(ColorStateList tint) {
        binding.containerCenterImage.setBackgroundTintList(tint);
    }

    public void setCenterImage(@DrawableRes int resId) {
        binding.centerImage.setImageResource(resId);
    }

    public void setIndicatorColor(ColorStateList tint) {
        binding.indicator.setBackgroundTintList(tint);
    }

    public void setIndicatorColorByPriority(NotePriority priority) {
        switch (priority) {
            case HIGH:
                setIndicatorColor(ColorStateList.valueOf(Color.RED));
                break;
            case MEDIUM:
                setIndicatorColor(ColorStateList.valueOf(Color.YELLOW));
                break;
            case LOW:
                setIndicatorColor(ColorStateList.valueOf(Color.GREEN));
                break;
        }
    }

    public void setBackgroundByPriority(NotePriority priority) {
        switch (priority) {
            case HIGH:
                setIndicatorColor(ColorStateList.valueOf(Color.RED));
                setCenterImage(R.drawable.ic_priority_high);
                break;
            case MEDIUM:
                setIndicatorColor(ColorStateList.valueOf(Color.YELLOW));
                setCenterImage(R.drawable.ic_priority_medium);
                break;
            case LOW:
                setIndicatorColor(ColorStateList.valueOf(Color.GREEN));
                setCenterImage(R.drawable.ic_priority_low);
                break;
        }
    }
}
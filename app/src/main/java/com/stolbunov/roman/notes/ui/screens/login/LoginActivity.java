package com.stolbunov.roman.notes.ui.screens.login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.stolbunov.roman.notes.R;
import com.stolbunov.roman.notes.databinding.ActivityLoginBinding;
import com.stolbunov.roman.notes.ui.screens.list.NotesListActivity;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerAppCompatActivity;

public class LoginActivity extends DaggerAppCompatActivity {
    @Inject
    LoginViewModelFactory factory;
    private LoginViewModel vm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vm = ViewModelProviders
                .of(this, factory)
                .get(LoginViewModel.class);

        ActivityLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setLifecycleOwner(this);
        binding.setVm(vm);

        observerData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (vm.isCurrentUser()) {
            updateUi(true);
        }
    }

    private void observerData() {
        vm.getIsSuccessfulAuthentication().observe(this, this::updateUi);
    }

    private void updateUi(Boolean isSuccess) {
        if (isSuccess != null && isSuccess) {
            Intent intent = new Intent(this, NotesListActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Authentication failed!", Toast.LENGTH_SHORT).show();
        }
    }
}

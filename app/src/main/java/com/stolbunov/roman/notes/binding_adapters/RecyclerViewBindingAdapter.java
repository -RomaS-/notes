package com.stolbunov.roman.notes.binding_adapters;

import com.stolbunov.roman.domain.entities.Note;
import com.stolbunov.roman.notes.ui.screens.list.INoteListAdapter;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewBindingAdapter {

    @BindingAdapter("items")
    public static void setItems(RecyclerView recyclerView, List<Note> notes) {
        INoteListAdapter adapter = ((INoteListAdapter) recyclerView.getAdapter());

        if (adapter != null && notes != null) {
            adapter.setDataList(notes);
        }
    }

    @BindingAdapter("itemTouchHelper")
    public static void setItemTouchHelper(RecyclerView recyclerView, ItemTouchHelper itemTouchHelper) {
        if (itemTouchHelper != null) {
            itemTouchHelper.attachToRecyclerView(recyclerView);
        }
    }
}

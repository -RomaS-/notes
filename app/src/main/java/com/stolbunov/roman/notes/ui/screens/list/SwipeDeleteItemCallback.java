package com.stolbunov.roman.notes.ui.screens.list;

import com.stolbunov.roman.notes.di.scope.ActivityScope;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

public class SwipeDeleteItemCallback extends ItemTouchHelper.SimpleCallback {
    private NotesListAdapter adapter;

    @Inject
    public SwipeDeleteItemCallback(@ActivityScope NotesListAdapter adapter) {
        super(0, ItemTouchHelper.START | ItemTouchHelper.END);
        this.adapter = adapter;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();
        adapter.removeItem(position);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
    }
}

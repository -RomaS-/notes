package com.stolbunov.roman.notes.ui.screens.list;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class NotesListViewModelFactory implements ViewModelProvider.Factory {
    private NotesListViewModel vm;

    @Inject
    public NotesListViewModelFactory(NotesListViewModel vm) {
        this.vm = vm;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        //noinspection unchecked
        return (T) vm;
    }
}

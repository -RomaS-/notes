package com.stolbunov.roman.notes.ui.screens.editor;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class NoteEditorViewModelFactory implements ViewModelProvider.Factory {
    private NoteEditorViewModel vm;

    @Inject
    NoteEditorViewModelFactory(NoteEditorViewModel vm) {
        this.vm = vm;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) vm;
    }
}

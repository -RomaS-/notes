package com.stolbunov.roman.notes.ui.screens.list;

import android.util.Log;

import com.stolbunov.roman.domain.entities.Note;
import com.stolbunov.roman.domain.interactor.AuthorizationInteractor;
import com.stolbunov.roman.domain.interactor.INoteInteractor;
import com.stolbunov.roman.notes.utils.RxLiveData;
import com.stolbunov.roman.notes.utils.SortingMethodFactory;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NotesListViewModel extends ViewModel {
    private int selectedMenuItemId;
    private final boolean isShowDetailsToWorkWithUser;

    private final INoteInteractor noteInteractor;
    private final AuthorizationInteractor auth;
    private final CompositeDisposable composite = new CompositeDisposable();

    private final MutableLiveData<List<Note>> notesList = new MutableLiveData<>();
    private final MutableLiveData<Comparator<Note>> sortingMethod = new MutableLiveData<>();
    private final MutableLiveData<List<Note>> sortedListNotes = new MutableLiveData<>();

    private final MutableLiveData<Boolean> isShowProgress = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isShowListNotes = new MutableLiveData<>();

    @Inject
    public NotesListViewModel(INoteInteractor noteInteractor, AuthorizationInteractor auth) {
        this.noteInteractor = noteInteractor;
        this.auth = auth;
        isShowDetailsToWorkWithUser = auth.isShowDetailsToWorkWithUser();

        sortingMethod.setValue(SortingMethodFactory.sortByPriority());
        getNotes();
        sortNotesList();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        composite.dispose();
    }

    void getNotes() {
        composite.add(noteInteractor.getNotesList()
                .doOnSubscribe(disposable -> showProgress(true))
                .doOnSuccess(list -> showProgress(false))
                .subscribeOn(Schedulers.io())
                .subscribe(notesList::postValue, this::logger));
    }

    void remove(String noteId) {
        composite.add(noteInteractor.remove(noteId)
                .subscribeOn(Schedulers.io())
                .subscribe(this::getNotes, this::logger));
    }

    public void signOut() {
        auth.signOut();
    }

    void setSortingMethod(Comparator<Note> sortingMethod) {
        this.sortingMethod.setValue(sortingMethod);
    }

    int getSelectedMenuItemId() {
        return selectedMenuItemId;
    }

    void setSelectedMenuItemId(int selectedMenuItemId) {
        this.selectedMenuItemId = selectedMenuItemId;
    }

    boolean isShowDetailsToWorkWithUser() {
        return isShowDetailsToWorkWithUser;
    }

    private void showProgress(boolean showProgress) {
        isShowProgress.postValue(showProgress);
        isShowListNotes.postValue(!showProgress);
    }

    private void sortNotesList() {
        composite.add(getSortedListNotesObservable()
                .subscribe(sortedListNotes::postValue));
    }

    private Observable<List<Note>> getSortedListNotesObservable() {
        return Observable.combineLatest(
                RxLiveData.toObservable(notesList)
                        .observeOn(Schedulers.computation()),
                RxLiveData.toObservable(sortingMethod)
                        .observeOn(Schedulers.computation()),
                (notesList, noteComparator) -> {
                    if (notesList != null) {
                        Collections.sort(notesList, noteComparator);
                    }
                    return notesList;
                });
    }

    private void logger(Throwable throwable) {
        Log.e("Notes", throwable.getMessage());
    }

    public LiveData<List<Note>> getSortedListNotes() {
        return sortedListNotes;
    }

    public LiveData<Boolean> getIsShowProgress() {
        return isShowProgress;
    }

    public LiveData<Boolean> getIsShowListNotes() {
        return isShowListNotes;
    }
}

package com.stolbunov.roman.notes.ui.screens.login;

import android.text.TextUtils;
import android.util.Log;

import com.stolbunov.roman.domain.entities.Credentials;
import com.stolbunov.roman.domain.interactor.AuthorizationInteractor;
import com.stolbunov.roman.notes.utils.RxLiveData;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends ViewModel {
    private final AuthorizationInteractor authorization;

    private final CompositeDisposable disposable = new CompositeDisposable();

    private final MutableLiveData<String> email = new MutableLiveData<>();
    private final MutableLiveData<String> password = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isValidated = new MutableLiveData<>();

    private final MutableLiveData<Boolean> isSuccessfulAuthentication = new MutableLiveData<>();

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }

    @Inject
    public LoginViewModel(AuthorizationInteractor authorization) {
        this.authorization = authorization;

        email.setValue("");
        password.setValue("");
        isValidated.setValue(false);

        disposable.add(isAllFieldsValid()
                .subscribe(isValidated::postValue));
    }

    public void registration() {
        disposable.add(
                authorization.createUserWithEmailAndPassword(getCredentials())
                        .subscribeOn(Schedulers.io())
                        .subscribe(isSuccessfulAuthentication::postValue, this::logger));
    }

    public void signing() {
        disposable.add(
                authorization.signInWithEmailAndPassword(getCredentials())
                        .subscribeOn(Schedulers.io())
                        .subscribe(isSuccessfulAuthentication::postValue, this::logger));
    }

    private Credentials getCredentials() {
        return new Credentials(email.getValue(), password.getValue());
    }

    private Observable<Boolean> isAllFieldsValid() {
        return Observable.combineLatest(
                RxLiveData.toObservable(email)
                        .observeOn(Schedulers.io()),
                RxLiveData.toObservable(password)
                        .observeOn(Schedulers.io()),
                (e, p) -> (!TextUtils.isEmpty(e) && !TextUtils.isEmpty(p) && p.length() > 5));
    }

    boolean isCurrentUser() {
        return authorization.isCurrentUser();
    }

    public MutableLiveData<String> getEmail() {
        return email;
    }

    public MutableLiveData<String> getPassword() {
        return password;
    }

    public LiveData<Boolean> getIsValidated() {
        return isValidated;
    }

    public LiveData<Boolean> getIsSuccessfulAuthentication() {
        return isSuccessfulAuthentication;
    }

    private void logger(Throwable throwable) {
        Log.e("Notes", throwable.getMessage());
    }
}

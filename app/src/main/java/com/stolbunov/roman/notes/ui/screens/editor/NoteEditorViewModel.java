package com.stolbunov.roman.notes.ui.screens.editor;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.stolbunov.roman.domain.entities.Note;
import com.stolbunov.roman.domain.entities.NotePriority;
import com.stolbunov.roman.domain.interactor.INoteInteractor;
import com.stolbunov.roman.notes.R;
import com.stolbunov.roman.notes.entities.Event;
import com.stolbunov.roman.notes.entities.Nothing;

import java.util.Date;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NoteEditorViewModel extends ViewModel {
    private final String noteId;
    private final Context context;
    private final INoteInteractor useCase;
    private final CompositeDisposable disposable = new CompositeDisposable();

    public final MutableLiveData<String> title = new MutableLiveData<>();
    public final MutableLiveData<String> description = new MutableLiveData<>();
    private final MutableLiveData<NotePriority> priority = new MutableLiveData<>();
    private final MutableLiveData<Date> expirationDate = new MutableLiveData<>();

    private final MutableLiveData<Event<String>> showMessage = new MutableLiveData<>();
    private final MutableLiveData<Event<Nothing>> isSuccessfulUpdate = new MutableLiveData<>();

    @Inject
    public NoteEditorViewModel(Context context, INoteInteractor useCase, String noteId) {
        this.context = context;
        this.useCase = useCase;
        this.noteId = noteId;

        fillingDefaultFields();
        getNote(noteId);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.dispose();
    }

    public void saveNote() {
        if (!isEmptyFields()) {
            Note note = getNoteFromFields();
            save(note);
        } else {
            showMessage.setValue(new Event<>(context.getString(R.string.message_empty_fields)));
        }
    }

    public void setPriority(NotePriority notePriority) {
        priority.setValue(notePriority);
    }

    private void fillingDefaultFields() {
        title.setValue("");
        description.setValue("");
        priority.setValue(NotePriority.MEDIUM);
    }

    private void fillingFields(Note note) {
        expirationDate.postValue(note.getExpirationDate());
        title.postValue(note.getTitle());
        description.postValue(note.getDescription());
        priority.postValue(note.getPriority());
    }

    private void getNote(String noteId) {
        if (!TextUtils.isEmpty(noteId)) {
            disposable.add(useCase.getNote(noteId)
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::fillingFields, this::reportError));
        }
    }

    private Note getNoteFromFields() {
        return new Note(noteId,
                title.getValue(),
                description.getValue(),
                priority.getValue(),
                new Date(),
                expirationDate.getValue());
    }

    private boolean isEmptyFields() {
        return TextUtils.isEmpty(title.getValue())
                || TextUtils.isEmpty(description.getValue())
                || expirationDate.getValue() == null;
    }

    private void save(Note note) {
        disposable.add(useCase.save(note)
                .subscribeOn(Schedulers.io())
                .subscribe(() -> isSuccessfulUpdate.postValue(
                        new Event<>(new Nothing())),
                        this::reportError));
    }

    private void reportError(Throwable throwable) {
        logger(throwable);
        showMessage.postValue(new Event<>(context.getString(R.string.message_save_failed)));
    }

    private void logger(Throwable throwable) {
        Log.e("Notes", "logger: " + throwable.getMessage());
    }

    public void setDate(Date date) {
        expirationDate.setValue(date);
    }

    public LiveData<Date> getExpirationDate() {
        return expirationDate;
    }

    public LiveData<NotePriority> getPriority() {
        return priority;
    }

    LiveData<Event<String>> getShowMessage() {
        return showMessage;
    }

    LiveData<Event<Nothing>> getIsSuccessfulUpdate() {
        return isSuccessfulUpdate;
    }
}

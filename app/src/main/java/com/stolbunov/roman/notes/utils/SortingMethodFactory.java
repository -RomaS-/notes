package com.stolbunov.roman.notes.utils;

import com.stolbunov.roman.domain.entities.Note;

import java.util.Comparator;

public final class SortingMethodFactory {

    private SortingMethodFactory() {
    }

    public static Comparator<Note> sortByTitle() {
        return (not1, not2) -> not1.getTitle().compareTo(not2.getTitle());
    }

    public static Comparator<Note> sortByPriority() {
        return (note1, note2) -> note1.getPriority().compareTo(note2.getPriority());
    }

    public static Comparator<Note> sortByLastChange() {
        return (note1, note2) -> {
            if (note1.getCreationDate().before(note2.getCreationDate())) {
                return 1;
            } else if (note1.getCreationDate().after(note2.getCreationDate())) {
                return -1;
            }
            return 0;
        };
    }

    public static Comparator<Note> sortByEndTime() {
        return (note1, note2) -> note1.getExpirationDate().compareTo(note2.getExpirationDate());
    }
}

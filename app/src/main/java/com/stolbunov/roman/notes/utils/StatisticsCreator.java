package com.stolbunov.roman.notes.utils;

import com.stolbunov.roman.domain.entities.Note;
import com.stolbunov.roman.domain.entities.NoteValidity;
import com.stolbunov.roman.notes.entities.NoteStatistics;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class StatisticsCreator {
    private static final int defaultTrackingRangeExpiredNotesInDays = 5;
    private int numberHighPriority;
    private int numberMediumPriority;
    private int numberLowPriority;
    private int numberInvalid;
    private int numberInvalidInFuture;
    private int numberChangedOnMonday;
    private int numberChangedOnTuesday;
    private int numberChangedOnWednesday;
    private int numberChangedToThursday;
    private int numberChangedToFriday;
    private int numberChangedToSaturday;
    private int numberChangedToSunday;

    @Inject
    public StatisticsCreator() {
    }

    public NoteStatistics create(List<Note> notes) {
        return create(notes, defaultTrackingRangeExpiredNotesInDays);
    }

    public NoteStatistics create(List<Note> notes, int trackingRangeExpiredNotesInDays) {
        for (Note note : notes) {
            gatheringInformationByPriorities(note);
            gatheringInformationByInvalid(note.getValidity());
            gatheringInformationByInvalidInFuture(note.getExpirationDate(), trackingRangeExpiredNotesInDays);
            gatheringInformationByDayOfWeek(note.getCreationDate());
        }
        return createNotesInfo();
    }

    private void gatheringInformationByPriorities(Note note) {
        switch (note.getPriority()) {
            case HIGH:
                numberHighPriority++;
                break;
            case MEDIUM:
                numberMediumPriority++;
                break;
            case LOW:
                numberLowPriority++;
        }
    }

    private void gatheringInformationByInvalid(NoteValidity validity) {
        if (validity.equals(NoteValidity.INVALID)) {
            numberInvalid++;
        }
    }

    private void gatheringInformationByInvalidInFuture(Date expirationDate, int trackingRangeExpiredNotesInDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, trackingRangeExpiredNotesInDays);
        if (calendar.getTime().after(expirationDate)) {
            numberInvalidInFuture++;
        }
    }

    private void gatheringInformationByDayOfWeek(Date creationDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(creationDate);

        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                numberChangedOnMonday++;
                break;
            case Calendar.TUESDAY:
                numberChangedOnTuesday++;
                break;
            case Calendar.WEDNESDAY:
                numberChangedOnWednesday++;
                break;
            case Calendar.THURSDAY:
                numberChangedToThursday++;
                break;
            case Calendar.FRIDAY:
                numberChangedToFriday++;
                break;
            case Calendar.SATURDAY:
                numberChangedToSaturday++;
                break;
            case Calendar.SUNDAY:
                numberChangedToSunday++;
                break;
        }
    }

    private NoteStatistics createNotesInfo() {
        return new NoteStatistics.Builder()
                .numberHighPriority(numberHighPriority)
                .numberMediumPriority(numberMediumPriority)
                .numberLowPriority(numberLowPriority)
                .numberInvalid(numberInvalid)
                .numberInvalidInFuture(numberInvalidInFuture)
                .numberChangedOnMonday(numberChangedOnMonday)
                .numberChangedOnTuesday(numberChangedOnTuesday)
                .numberChangedOnWednesday(numberChangedOnWednesday)
                .numberChangedToThursday(numberChangedToThursday)
                .numberChangedToFriday(numberChangedToFriday)
                .numberChangedToSaturday(numberChangedToSaturday)
                .numberChangedToSunday(numberChangedToSunday)
                .build();
    }
}
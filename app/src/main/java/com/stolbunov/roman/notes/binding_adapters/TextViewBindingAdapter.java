package com.stolbunov.roman.notes.binding_adapters;

import android.widget.TextView;

import com.stolbunov.roman.domain.entities.NotePriority;
import com.stolbunov.roman.notes.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.databinding.BindingAdapter;

public class TextViewBindingAdapter {
    @BindingAdapter(value = {"date", "detailsForDate"}, requireAll = false)
    public static void setTextDate(TextView textView, Date date, String detailsForDate) {

        if (date != null) {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy  HH:mm", Locale.US);
            String dateString = format.format(date);
            if (detailsForDate != null) {
                textView.setText(String.format(detailsForDate, dateString));
            } else {
                textView.setText(dateString);
            }
        }
    }

    @BindingAdapter(value = {"notePriority"})
    public static void setNotePriority(TextView textView, NotePriority priority) {
        if (priority != null) {
            String priorityText = "";
            switch (priority) {
                case HIGH:
                    priorityText = textView.getContext().getString(R.string.high);
                    break;
                case MEDIUM:
                    priorityText = textView.getContext().getString(R.string.medium);
                    break;
                case LOW:
                    priorityText = textView.getContext().getString(R.string.low);
                    break;
            }
            textView.setText(priorityText);
        }
    }

    @BindingAdapter(value = {"textStatistics", "valueStatistics"})
    public static void setInformation(TextView textView, String formatText, Integer value) {
        if (formatText != null) {
            Integer result = 0;
            if (value != null) {
                result = value;
            }
            textView.setText(String.format(formatText, result));
        }
    }
}

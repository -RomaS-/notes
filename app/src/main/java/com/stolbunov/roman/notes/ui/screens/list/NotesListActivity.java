package com.stolbunov.roman.notes.ui.screens.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.stolbunov.roman.notes.R;
import com.stolbunov.roman.notes.databinding.ActivityNotesListBinding;
import com.stolbunov.roman.notes.di.scope.ActivityScope;
import com.stolbunov.roman.notes.ui.screens.editor.NoteEditorActivity;
import com.stolbunov.roman.notes.ui.screens.login.LoginActivity;
import com.stolbunov.roman.notes.ui.screens.statistics.NoteStatisticsActivity;
import com.stolbunov.roman.notes.utils.SortingMethodFactory;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import dagger.android.support.DaggerAppCompatActivity;

public class NotesListActivity extends DaggerAppCompatActivity
        implements NotesListAdapter.OnRemoveNoteListener, NotesListAdapter.OnItemClickListener {

    private final static int RC_NOTE_EDIT = 351;
    @Inject
    ItemTouchHelper itemTouchHelper;
    @ActivityScope
    @Inject
    NotesListAdapter adapter;
    @Inject
    NotesListViewModelFactory viewModelFactory;

    private NotesListViewModel vm;

    public static Intent getIntent(Context context) {
        return new Intent(context, NotesListActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vm = ViewModelProviders.of(this, viewModelFactory).get(NotesListViewModel.class);

        ActivityNotesListBinding binding = DataBindingUtil.setContentView(
                this, R.layout.activity_notes_list);
        binding.setLifecycleOwner(this);
        binding.setVm(vm);
        binding.setSwipeRemoveItem(itemTouchHelper);

        binding.recyclerView.setAdapter(adapter);
        binding.createNote.setOnClickListener(this::createNote);

        setSupportActionBar(binding.bottomAppBar);
        initAdapterListener();
    }

    @Override
    public void onDeleteItemClick(String noteId) {
        vm.remove(noteId);
    }

    @Override
    public void onItemClick(String noteId) {
        editNote(noteId);
    }

    private void initAdapterListener() {
        adapter.setItemClickListener(this);
        adapter.setRemoveListener(this);
    }

    private void editNote(String noteId) {
        Intent intent = NoteEditorActivity.getIntent(this, noteId);
        startActivityForResult(intent, RC_NOTE_EDIT);
    }

    public void createNote(View view) {
        editNote("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == RC_NOTE_EDIT) {
                vm.getNotes();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_notes_list, menu);
        checkSelectedMenuItem(menu);

        if (vm.isShowDetailsToWorkWithUser()) {
            MenuItem item = menu.findItem(R.id.log_out);
            item.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        item.setChecked(true);
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.sort_priority:
                vm.setSortingMethod(SortingMethodFactory.sortByPriority());
                vm.setSelectedMenuItemId(itemId);
                return true;
            case R.id.sort_title:
                vm.setSortingMethod(SortingMethodFactory.sortByTitle());
                vm.setSelectedMenuItemId(itemId);
                return true;
            case R.id.sort_last_change:
                vm.setSortingMethod(SortingMethodFactory.sortByLastChange());
                vm.setSelectedMenuItemId(itemId);
                return true;
            case R.id.sort_end_date:
                vm.setSortingMethod(SortingMethodFactory.sortByEndTime());
                vm.setSelectedMenuItemId(itemId);
                return true;
            case R.id.note_statistics:
                Intent intent = NoteStatisticsActivity.getIntent(this);
                startActivity(intent);
                return true;
            case R.id.log_out:
                vm.signOut();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void checkSelectedMenuItem(Menu menu) {
        int selectedMenuItemId = vm.getSelectedMenuItemId();
        if (selectedMenuItemId > 0) {
            menu.findItem(selectedMenuItemId).setChecked(true);
        } else {
            menu.findItem(R.id.sort_priority).setChecked(true);
        }
    }
}

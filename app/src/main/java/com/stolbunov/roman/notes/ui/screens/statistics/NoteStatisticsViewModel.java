package com.stolbunov.roman.notes.ui.screens.statistics;

import android.util.Log;

import com.stolbunov.roman.domain.entities.Note;
import com.stolbunov.roman.domain.interactor.INoteInteractor;
import com.stolbunov.roman.notes.entities.NoteStatistics;
import com.stolbunov.roman.notes.utils.StatisticsCreator;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NoteStatisticsViewModel extends ViewModel {
    private final INoteInteractor noteInteractor;
    private final StatisticsCreator statisticsCreator;

    private final CompositeDisposable composite = new CompositeDisposable();

    private final MutableLiveData<List<Note>> notesList = new MutableLiveData<>();
    private final MutableLiveData<NoteStatistics> notesStatistics = new MutableLiveData<>();

    @Inject
    public NoteStatisticsViewModel(INoteInteractor noteInteractor, StatisticsCreator statisticsCreator) {
        this.noteInteractor = noteInteractor;
        this.statisticsCreator = statisticsCreator;

        getNotes();
        notesList.observeForever(this::createNoteStatistics);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        composite.dispose();
    }

    void getNotes() {
        composite.add(noteInteractor.getNotesList()
                .subscribeOn(Schedulers.io())
                .subscribe(notesList::postValue, this::logger));
    }

    private Single<NoteStatistics> getNoteInfo(List<Note> notes) {
        return Single.fromCallable(() -> statisticsCreator.create(notes));
    }

    private void createNoteStatistics(List<Note> notes) {
        if (notes != null) {
            composite.add(getNoteInfo(notes)
                    .subscribeOn(Schedulers.computation())
                    .subscribe(notesStatistics::postValue, this::logger));
        }
    }

    public LiveData<NoteStatistics> getNotesStatistics() {
        return notesStatistics;
    }

    private void logger(Throwable throwable) {
        Log.e("Notes", throwable.getMessage());
    }
}

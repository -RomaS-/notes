package com.stolbunov.roman.notes.di;

import com.stolbunov.roman.notes.ui.screens.editor.NoteEditorActivity;

import dagger.Module;
import dagger.Provides;

@Module
class NoteEditorModule {

    @Provides
    String provideNoteId(NoteEditorActivity activity) {
        return activity.getNoteId();
    }
}

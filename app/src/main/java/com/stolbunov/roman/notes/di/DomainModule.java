package com.stolbunov.roman.notes.di;

import com.stolbunov.roman.domain.interactor.INoteInteractor;
import com.stolbunov.roman.domain.interactor.NoteInteractor;

import dagger.Binds;
import dagger.Module;

@Module
public interface DomainModule {

    @Binds
    INoteInteractor provideNoteListInteractor(NoteInteractor noteInteractor);
}

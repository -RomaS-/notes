package com.stolbunov.roman.notes.ui.screens.statistics;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.stolbunov.roman.notes.R;
import com.stolbunov.roman.notes.databinding.ActivityNoteStatisticsBinding;
import com.stolbunov.roman.notes.entities.NoteStatistics;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerAppCompatActivity;

public class NoteStatisticsActivity extends DaggerAppCompatActivity implements NoteStatisticsFragment.OnClickListener {
    private NoteStatisticsFragment fragment;
    private ActivityNoteStatisticsBinding binding;
    private NoteStatisticsViewModel vm;

    @Inject
    NoteStatisticsViewModelFactory factory;

    public static Intent getIntent(Context context) {
        return new Intent(context, NoteStatisticsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vm = ViewModelProviders
                .of(this, factory)
                .get(NoteStatisticsViewModel.class);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_note_statistics);
        binding.setLifecycleOwner(this);
        binding.setVm(vm);

        fragment = NoteStatisticsFragment.newInstance();
        showNoteStatisticsFragment();

        vm.getNotesStatistics().observe(this, this::showStatistics);
    }

    private void showStatistics(NoteStatistics statistics) {
        fragment.setNoteStatistics(statistics);
    }

    private void showNoteStatisticsFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.replace(R.id.containerStatistics, fragment, "NoteStatisticsFragment");
        transaction.commit();
    }

    @Override
    public void onHideClick() {
        finish();
    }
}

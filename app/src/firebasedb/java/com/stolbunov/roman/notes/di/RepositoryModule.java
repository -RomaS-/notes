package com.stolbunov.roman.notes.di;

import com.stolbunov.roman.data_firebasedb.repository.FirebaseRepository;
import com.stolbunov.roman.domain.repository.INoteRepository;
import com.stolbunov.roman.notes.di.scope.AppScope;

import dagger.Binds;
import dagger.Module;

@Module
public interface RepositoryModule {
    @AppScope
    @Binds
    INoteRepository provideRepositoryManager(FirebaseRepository repository);
}

package com.stolbunov.roman.notes.di;

import com.stolbunov.roman.data_sqlitedb.repository.SQLiteNoteRepository;
import com.stolbunov.roman.domain.repository.INoteRepository;
import com.stolbunov.roman.notes.di.scope.AppScope;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module(includes = MigrationDatabaseModule.class)
public class RepositoryModule {
    @AppScope
    @Provides
    INoteRepository provideRepositoryManager(SQLiteNoteRepository repository) {
        return repository;
    }

    @AppScope
    @Provides
    ReentrantReadWriteLock provideReentrantReadWriteLock() {
        return new ReentrantReadWriteLock();
    }
}

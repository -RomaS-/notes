package com.stolbunov.roman.notes.di;

import com.stolbunov.roman.data_sqlitedb.repository.database.migration.IMigration;
import com.stolbunov.roman.data_sqlitedb.repository.database.migration.CreateNotesTableMigration;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;

@Module
public interface MigrationDatabaseModule {

    @IntoSet
    @Binds
    IMigration provideCreateNotesMigration(CreateNotesTableMigration createNotesTableMigration);

}

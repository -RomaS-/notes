package com.stolbunov.roman.notes.di;

import com.stolbunov.roman.data_sqlitedb.authorization.NoteSQLiteAuth;
import com.stolbunov.roman.domain.repository.INoteRepositoryAuth;

import dagger.Binds;
import dagger.Module;

@Module
public interface AuthorizationModule {
    @Binds
    INoteRepositoryAuth provideAuthorization(NoteSQLiteAuth authorizationSQLite);
}
